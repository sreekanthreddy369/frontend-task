import React, { useState, createContext } from 'react'
import data from '../data.json'


export const AppContext = createContext()

export const AppProvider = props => {

    const [appData, setAppData] = useState(data)
    return (
        <AppContext.Provider value={[appData, setAppData]}>
            {props.children}
        </AppContext.Provider>
    )
}
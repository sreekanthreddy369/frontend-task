import React from "react"
import Image from "./children/image"
import styles from "./styles"
import { AppProvider } from "./AppContext"
import { CommentsBlock } from "./children/comments_block"
import UserBlock from "./children/user_block"
import UserFooterDetails from "./children/user_block/UserFooterDetails"

const App = () => {
  return (
    <AppProvider>
      <main style={styles.main}>
        <div className="img-container">
          <Image />
        </div>
        <div className="user-post-details-container ">
          <UserBlock />
          <div className="comments-container">
            <CommentsBlock />
          </div>
          <UserFooterDetails />
        </div>
      </main>
    </AppProvider>
  );
};

export default App;

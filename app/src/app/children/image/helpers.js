import { get } from 'lodash'

export function getCaptionFromEdges (data) {
  const text = get(data, 'edges[0].node.text', '')
  return text
}

import React, { useContext } from 'react'
import styles from './styles'
import {getCaptionFromEdges} from './helpers'
import { AppContext } from '../../AppContext'

const Image = () => {

  const [appData, setAppData] = useContext(AppContext)
  const { display_url, edge_media_to_caption } = appData
  return (
    <img
      src={display_url}
      style={styles}
      alt={getCaptionFromEdges(edge_media_to_caption)}
    />
  )
}

export default Image

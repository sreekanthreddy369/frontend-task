import React, { useContext } from "react"
import { get } from "lodash"
import { AppContext } from "../../AppContext"
import { LikeButton } from "../like_button"
import { UnlikeButton } from "../like_button"
import HashTag from '../hashtag'

const Comment = props => {
  const {
    node: {
      text = "",
      viewer_has_liked = false,
      edge_liked_by: { count = 0 },
      owner: { profile_pic_url = "", username = "" }
    }
  } = props.data
  return (
    <div className="comment-section-container">
      <div className="comment-section">
        <span className="commented-username"> {username}</span> <HashTag text={text} />
        <p className="comment-likes"> {count} likes</p>
      </div>
      <div className="comment-like-section">
        {viewer_has_liked ? <LikeButton /> : <UnlikeButton />}
      </div>
    </div>
  );
};

export const CommentsBlock = () => {
  const [appData, setAppData] = useContext(AppContext);
  const comments = get(appData, "edge_media_to_comment.edges", []);

  return comments.map(comment => (
    <Comment key={comment.node.id} data={comment} />
  ));
};

import React, { useContext } from "react"
import { AppContext } from "../../AppContext"
import { LikeButton } from "../like_button"
import { UnlikeButton } from "../like_button"
import { numberFormat } from "./helpers"

const UserFooterDetails = () => {
  const [appData, setAppData] = useContext(AppContext)

  const {
    viewer_has_liked = false,
    edge_media_preview_like: { count = 0 },
    taken_at_timestamp
  } = appData

  let dateObj = new Date(taken_at_timestamp * 1000)

  return (
    <div className="footer-details">
      <div className="info-section">
        <h1 className="user-name"> {numberFormat(count)} likes</h1>
        <p className="user-address">
          On {dateObj.toISOString().split("T")[0]}{" "}
        </p>
      </div>
      <div className="like-now-section">
        {viewer_has_liked ? <LikeButton /> : <UnlikeButton />}
      </div>
    </div>
  );
};

export default UserFooterDetails

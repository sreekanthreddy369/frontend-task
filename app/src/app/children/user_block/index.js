import React, { useContext } from "react"
import { AppContext } from "../../AppContext"
import HashTag from '../hashtag'

const UserBlock = () => {
  const [appData, setAppData] = useContext(AppContext)
  const {
    edge_media_to_caption: {
      edges: [
        {
          node: { text = "" }
        }
      ]
    },
    location: { name = "" },
    owner: { username = "", profile_pic_url = "" }
  } = appData

  return (
    <>
      <div className="user-block-container">
        <div className="user-avatar">
          <img src={profile_pic_url} alt="User Profile" ></img>
        </div>
        <div className="user-details">
          <h1 className="user-name"> {username}</h1>
          <p className="user-address"> {name} </p>
        </div>
      </div>
      <hr />
      <div className="post-caption">
        <span className="commented-username"> {username}</span> <HashTag text={text} />
      </div>
    </>
  );
};

export default UserBlock

import React from 'react'

const HashTag = ({text}) => {

    text = text
      .split(" ")
      .map(ele => {
        if (ele.includes("#")) {
          return ele.replace(/#(\w+)/g, `<span class="hashtag">#$1</span>`)
        } else if (ele.includes("@")) {
          return ele.replace(/@(\w+)/g, `<span class="hashtag">@$1</span>`)
        } else return ele
      })
      .join(" ")

    return (
        <span
        dangerouslySetInnerHTML={{
          __html : text
        }} />
    )
}

export default HashTag
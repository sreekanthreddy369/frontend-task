import React from 'react'
import LikedIcon from './children/liked_icon'
import UnlikedIcon from './children/unliked_icon'

export const LikeButton = () => {
  return <LikedIcon />
}

export const UnlikeButton = () => {
  return <UnlikedIcon />
}
